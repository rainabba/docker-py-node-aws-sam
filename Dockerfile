FROM python:3.7-slim-stretch

LABEL MAINTAINER="Michael Richardson <michael@privacymaxx.com>"

ARG DOCKER_GID=1000
ENV SHELL=/bin/bash

COPY src/. /home/docker/src

RUN export TERM=xterm-color; set -ae; \
  printf "::Starting Docker build\n\n"; \
  apt-get -yq update && apt-get -yq install sudo; \
  PASSWORD=$(openssl passwd -1 PASSWORD) && printf "::PASSWORD='%s'\n" $PASSWORD; \
  useradd -u ${DOCKER_GID} -Um -G sudo -s /bin/bash -p ${PASSWORD} docker; \
  echo 'docker  ALL=(ALL:ALL) NOPASSWD: ALL' >> /etc/sudoers; \
  echo "::SUDO Check!!"; \
  set -e;

RUN apt-get -yq install \
    libxslt1-dev \
    libxml2 \
    libsm6 \
    libglib2.0-0 \
    g++ gcc git \
    libffi-dev libssl-dev \
    dialog apt-utils unzip ssh curl git \
    -o APT::Install-Suggests=0 -o APT::Install-Recommends=0; \
  \
  curl -sSL https://get.docker.com/ | sh \
  && pip install --ignore-installed -U pip setuptools; \
  set -e; apt-get update; 

COPY src/.bashrc /home/docker/.bashrc
COPY src/.profile /home/docker/.profile
COPY src/.profile /home/root/.profile

USER docker
WORKDIR /home/docker

RUN sudo chown -R docker:root /home/docker && sh src/02_install-deps.sh

CMD /bin/bash