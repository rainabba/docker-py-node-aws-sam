# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [UNRELEASED]
### Changed

- Added Java
- Switched from Node to NVM and install --lts to again provide Node.js
- New pipeline workflow rules

## [1.1.10]

### Changed

- Added "REVISION" to publishing tags
- Add step to install git in preinstall if it's not detected

### Fixed

    - .preinstall script was running before node was installed and needed a check first
    - Set executable permissions on needed files

## [1.0.9]

### Changed

- Cleanup of extraneous commands in Dockerfile

## [1.0.9]

### Added

- Checking at the end of 01_prepare-environment.sh for ~/.bashrc and sourcing if it exists

### Changed

- Setup publishing of image to GitLab when executed on GitLab
- Dockerfile calling method for 02_install-deps.sh

### Fixed

- Permissions on /home/docker

## [1.0.8]

### Fixed

- Script was trying to copy package.json in preinstall instead of build step

## [1.0.7]

### Changed

- Cleanup and refactoring

## [1.0.6]

### Changed

- Move env-var init code in 01_prepare-environment so it will also get run in the build process

## [1.0.5]

### Changed

- Removed all references to AWS_PROFILE to ensure dependency on ~/.aws or GitLab AWS env-vars

## [1.0.4]

- Cleanup/Version-bump

## [1.0.3]

### Changed

- Minor improvements

### Fixed

- Environment was not exposing some intended functions when run via entrypoint
- PATH was not set properly for root and docker users

## [1.0.2]

### Added

- Entrypoint script to call 01_prepare_environment.sh before bash, then execute the command passed to `docker run`

### Changed

- Refined logging output
- Updated README>md
- Renamed awsEcr\*\*\* env-vars to use AWS*ECR* pattern
- New plan for .gitlab-ci.yml implemented
- Removed REVISION param/tag

## [1.0.1]

### Added

- Included NPM dependency check

## [1.0.0]

### Changed

- Version bump and commit to test GitLab pipeline
- Fix rule to reference branch name for initial testing.
- Attempt to fix GitLab authentication
- Remove unncessary chmod call
- Update .gitlab-ci.yml to use the same base-image as the Dockerfile
- Remove cache from .gitlab-ci.yml for now

## [0.9.5]

### Added

- Initial commit
