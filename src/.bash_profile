export TERM=xterm-color;
export PATH="$PATH:$HOME";

if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

export HOMEBREW_PREFIX="/home/linuxbrew/.linuxbrew";
export HOMEBREW_CELLAR="/home/linuxbrew/.linuxbrew/Cellar";
export HOMEBREW_REPOSITORY="/home/linuxbrew/.linuxbrew/Homebrew";
export MANPATH="/home/linuxbrew/.linuxbrew/share/man${MANPATH+:$MANPATH}:";
export INFOPATH="/home/linuxbrew/.linuxbrew/share/info${INFOPATH+:$INFOPATH}"
export PATH="$PATH:/usr/local/bin:/usr/local/sbin:/usr/sbin:/usr/bin:/sbin:/bin:/bin:/root/.local/bin:/home/docker/.local/bin:/home/docker/.yarn/bin:/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:/home/docker/.local/bin:/home/linuxbrew/.linuxbrew/opt/node@14/bin:/home/docker/.local/bin:/home/linuxbrew/.linuxbrew/opt/openjdk@8/bin";
export CPPFLAGS="-I/home/linuxbrew/.linuxbrew/opt/openjdk@8/include";

cd $CI_PROJECT_DIR;