TERM=xterm-color;
export TERM;

export PATH="/usr/local/bin:/usr/local/sbin:/usr/sbin:/usr/bin:/sbin:/bin:/bin:/root/.local/bin:~/.local/bin:~/.yarn/bin:/home/linuxbrew/.linuxbrew/bin/:/home/linuxbrew/.linuxbrew/Cellar/node@12/12.18.0/bin:/home/linuxbrew/.linuxbrew/sbin:/home/docker/.local/bin:/home/linuxbrew/.linuxbrew/opt/openjdk@8/bin";
export CPPFLAGS="-I/home/linuxbrew/.linuxbrew/opt/openjdk@8/include";
[ -s "/home/linuxbrew/.linuxbrew/opt/nvm/nvm.sh" ] && . "/home/linuxbrew/.linuxbrew/opt/nvm/nvm.sh"  # This loads nvm
[ -s "/home/linuxbrew/.linuxbrew/opt/nvm/etc/bash_completion.d/nvm" ] && . "/home/linuxbrew/.linuxbrew/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion