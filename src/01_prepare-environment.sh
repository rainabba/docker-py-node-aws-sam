#!/usr/bin/env bash
set -eu +a;

source src/color.sh

# set -ae; # Exit on all errors and exports all set variables to env

RUN_ARGS="$*";
printf "\n${YELLOW}::>>${NC} RUN_ARGS: \`%s\`\n" "$RUN_ARGS";

PATH=/usr/local/bin:/usr/local/sbin:/usr/sbin:/usr/bin:/sbin:/bin:/bin:/root/.local/bin:/home/docker/.local/bin:/home/docker/.yarn/bin:/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:/home/docker/.local/bin:/home/linuxbrew/.linuxbrew/opt/node@12/bin;

if hash node 2>/dev/null; then
    NODE_VERSION="$(node --version)";
    if [ -n "${NODE_VERSION}" ]; then
        NODE_VERSION="${NODE_VERSION#v}";
    fi
else
    NODE_VERSION="12.18.2"; #Done for clarity. Shouldn't matter until something tries to use node, which shouldn't happen after the image is built.
fi
NODE_VERSION_MAJOR=${NODE_VERSION%%.*};
PATH=$PATH:/home/linuxbrew/.linuxbrew/Cellar/node@${NODE_VERSION_MAJOR}/${NODE_VERSION#v}/bin;

TERM=xterm-color
DEBIAN_FRONTEND=noninteractive
DEBCONF_NONINTERACTIVE_SEEN=true;
RUN_DATETIME=$(date +%Y%m%dT%H%M%S);

function loadEnv() {
    if [ -f ".${ENV}.env" ]; then printf "\n${YELLOW}::>>${NC} Sourcing .%s.env\n" "$ENV" && set -a; source ."$ENV".env; set +a; fi
    
    if [ -z "${CI_REGISTRY:-}" ]; then # echo "true"; else echo "false"; fi;
        printf "\n:Missing environment variable: CI_REGISTRY\n;"
        exit 3;
    fi

    if [ -z "${CI_REGISTRY_USER}" ]; then
        printf "\n:Missing environment variable: CI_REGISTRY_USER\n;"
        exit 3;
    fi  

    printf "\n${YELLOW}::>>${NC} PWD=%s\n" "$(pwd)";
    cd "${CI_PROJECT_DIR}";
}
export -f loadEnv


function preInstall() {
    loadEnv;
    mkdir -p /tmp/images;
    if [ -f package.json ]; then
        TAG="node${NODE_VERSION_MAJOR}-$(grep version < package.json | head -1 | awk -F: '{ print $2 }' | sed 's/[\",]//g' | tr -d '[[:space:]]')";
    else
        TAG="";
    fi
    CONTAINER_NAME="py-node-aws-sam_${TAG//./_}";
    REVISION=${CI_COMMIT_SHA:-DEVLOCAL}
    printf "\n${YELLOW}::>>${NC} CONTAINER_NAME=%s, REVISION=%s\n" "${CONTAINER_NAME}" "${REVISION}";
}
export -f preInstall


function buildImage() {
    set -eu;
    preInstall;

    if [ -z "${IMAGE_NAME:-}" ]; then # echo "true"; else echo "false"; fi;
        printf "\n:Missing environment variable: IMAGE_NAME\n;"
        exit 3;
    fi

     if [ -n "${DEBUG:-}" ]; then printf "::\nDOCKER_INFO...\n"; docker info; docker ps -a; fi

    printf "\n${YELLOW}::>>${NC} Building Docker CI_REGISTRY=%s, CI_REGISTRY_USER=%s, IMAGE_NAME=%s, TAG=%s, CI_REGISTRY_IMAGE=%s, CI_PROJECT_DIR=%s at %s\n\n" "${CI_REGISTRY}" "${CI_REGISTRY_USER}" "${IMAGE_NAME}" "${TAG}" "${CI_REGISTRY_IMAGE}" "${CI_PROJECT_DIR}" "${RUN_DATETIME}";

    docker build \
        --cache-from "${IMAGE_NAME}":latest \
        -t "${IMAGE_NAME}":latest \
        .;
    BUILD_EXIT_CODE=$?
    if [ $BUILD_EXIT_CODE -ne 0 ]; then exit $BUILD_EXIT_CODE; fi
    addTags;
}
export -f buildImage;

function addTags() {
    docker tag "${IMAGE_NAME}":latest "${CI_REGISTRY_USER}"/"${IMAGE_NAME}":latest;
    docker tag "${IMAGE_NAME}":latest "${CI_REGISTRY_IMAGE}":latest;

    if [ -n "${TAG}" ]; then
        docker tag "${IMAGE_NAME}":latest "${IMAGE_NAME}":"${TAG}";
        docker tag "${IMAGE_NAME}":latest "${CI_REGISTRY_USER}"/"${IMAGE_NAME}":"${TAG}";
        docker tag "${IMAGE_NAME}":latest "${CI_REGISTRY_IMAGE}":"${TAG}";
    fi

    if [ -n "${REVISION}" ]; then
        docker tag "${IMAGE_NAME}":latest "${IMAGE_NAME}":"${REVISION}";
        docker tag "${IMAGE_NAME}":latest "${CI_REGISTRY_USER}"/"${IMAGE_NAME}":"${REVISION}";
        docker tag "${IMAGE_NAME}":latest "${CI_REGISTRY_IMAGE}":"${REVISION}";
    fi
}
export -f addTags;

function runContainer() {
    loadEnv;
    printf "\n${YELLOW}::>>${NC} Starting Docker Environment for .gitlab-ci.yml dev work at %s\n\n" "${RUN_DATETIME}";

    if [ "$(docker ps -qa -f name=${CONTAINER_NAME})" ]; then
        echo "${YELLOW}::>>${NC} Found container - ${CONTAINER_NAME}"
        if [ "$(docker ps -q -f name=${CONTAINER_NAME})" ]; then
            echo "${YELLOW}::>>${NC} Stopping running container - ${CONTAINER_NAME}";
            docker stop ${CONTAINER_NAME};
        fi
        echo "${YELLOW}::>>${NC} Removing stopped container - ${CONTAINER_NAME}";
        docker rm ${CONTAINER_NAME};
    fi

    if [ -z "$*" ]; then
        runArgs="/bin/bash";
    else
        runArgs="$*";
    fi

    set -x;
    docker run --privileged -t -i \
        --name "$CONTAINER_NAME" \
        --env-file ./."$ENV".env \
        -e TESTING \
        -e DOCKER_TLS_CERTDIR="${DOCKER_TLS_CERTDIR-}" \
        -e DOCKER_HOST="unix:///var/run/docker.sock" \
        -e PATH \
        -v /var/run/docker.sock:/var/run/docker.sock \
        -v "$PWD":"$CI_PROJECT_DIR"\
        --mount type=bind,source="$(realpath ~/.aws)",target=/home/docker/.aws,readonly \
        -v "$HOME"/.ssh:/home/docker/.ssh \
        -v /var/run/docker.sock:/var/run/docker.sock \
        "$IMAGE_NAME":"$TAG" \
        "$runArgs";
}
export -f runContainer;

function publishImage() {   
    loadEnv;

    if [ -z "${CI_REGISTRY_IMAGE}" ]; then
        printf "\n:Missing environment variable: CI_REGISTRY_IMAGE\n;"
        exit 3;
    fi

    printf "\n${YELLOW}::>>${NC} Publishing %s:latest} at %s\n" "${CI_REGISTRY_IMAGE}" "${RUN_DATETIME}";
    docker push "$CI_REGISTRY_IMAGE:latest";

    if [ -n "$TAG" ]; then
        printf "\n${YELLOW}::>>${NC} Publishing %s:%s\n" "${CI_REGISTRY_IMAGE}" "${TAG}";
        docker push "${CI_REGISTRY_IMAGE}":"${TAG}";
    fi

    if [ -n "$REVISION" ]; then
    printf "\n${YELLOW}::>>${NC} Publishing %s:\n" "${CI_REGISTRY_IMAGE}" "${REVISION}";
        docker push "${CI_REGISTRY_IMAGE}":"${REVISION}";
    fi
}
export -f publishImage;

function checkTestAWS() {
    printf "::%s\t == %s\n" "SAM   " "$(sam --version)";
    printf "::%s\t == %s\n" "AWS   " "$(aws --version)";
    printf "::%s\t == %s\n" "NODE  " "$(node --version)";
    printf "::%s\t == %s\n" "NPM   " "$(npm --version)";
    printf "::%s\t == %s\n" "JQ    " "$(jq --version)";
    printf "::%s\t == %s\n" "DOCKER" "$(docker --version)"; # Provided by the context image with dind (Docker in Docker)

    printf "\n::Executing IAM info:\n"
    aws sts get-caller-identity;

    ACCOUNT_ID=$(aws sts get-caller-identity --query 'Account' --output=text);
    AWS_REGION=$(aws configure get region --output=text);
    printf "\n\nACCOUNT_ID=%s, REGION=%s\n\n" "$ACCOUNT_ID" "$AWS_REGION";

    # Check if the account id is valid
    if ! [ "$ACCOUNT_ID" -gt 9999999999 ]; then
        printf "\n${YELLOW}::>>${NC} INVALID AccountID\n\n!!";
        exit 1;
    else
        printf "\n${YELLOW}::>>${NC} Validated AWS AccountID\n\n";
    fi
}
export -f checkTestAWS;

function dangerFreeSpace() {
    read -p "This will agressively free up disk space used by Docker! Are you sure? " -n 1 -r
    echo    # (optional) move to a new line
    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        exit 1;
    fi
    docker images --no-trunc | grep '<none>' | awk '{ print $3 }' | xargs -r docker rmi;
}
export -f dangerFreeSpace

function freeSpace() {
    docker ps --filter status=dead --filter status=exited -aq | xargs docker rm -v;
}
export -f freeSpace

function _should_tls() {
	[ -n "${DOCKER_TLS_CERTDIR:-}" ] \
	&& [ -s "$DOCKER_TLS_CERTDIR/client/ca.pem" ] \
	&& [ -s "$DOCKER_TLS_CERTDIR/client/cert.pem" ] \
	&& [ -s "$DOCKER_TLS_CERTDIR/client/key.pem" ]
}
export -f _should_tls

if [ -z "$RUN_ARGS" ]; then
    printf "\n\tNo command provided. This is normal to see during init.\n\n${YELLOW}::>>${NC} EXECUTING: /bin/bash\n\n";
    $SHELL
else
    if [ -n "${DEBUG:-}" ]; then
        printf "\n${YELLOW}::>>${NC} PWD=%s\n" "$(pwd)";
        printf "\n${YELLOW}::>>${NC} LS=";
        ls -lah --color;
        printf "\n\n";
    fi
    printf "\n${YELLOW}::>>${NC} EXECUTING: \'%s\'\n" "${RUN_ARGS}";
    bash -c "$RUN_ARGS";
fi
