#!/usr/bin/env bash
set -a
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color
CALLOUT="${YELLOW}::>>${NC}"
FAILED="${RED}FAILED !!!${NC}"
SUCCESS="${GREEN}SUCCESS :)${NC}"