#!/usr/bin/env bash
set -aue
source src/color.sh

ENV="${CI_COMMIT_BRANCH:-develop}"
if [ -f ".${ENV}.env" ]; then source .${ENV}.env; fi
  
if [ -n "${DEBUG:-}" ]; then pwd; ls -lah; fi
source src/01_prepare-environment.sh
preInstall # Sets REVISION
printf "\n${YELLOW}::>>${NC} Logging into docker registry...\n"; echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" "$CI_REGISTRY" --password-stdin