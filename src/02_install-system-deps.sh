#!/usr/bin/env bash
set -ae;

export TERM=xterm-color;
export DEBIAN_FRONTEND=noninteractive;
export DEBCONF_NONINTERACTIVE_SEEN=true;
export PATH=/usr/local/bin:/usr/local/sbin:/usr/sbin:/usr/bin:/sbin:/bin:/bin:/root/.local/bin:/home/docker/.local/bin:/home/docker/.yarn/bin:/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:/home/docker/.local/bin:/home/linuxbrew/.linuxbrew/opt/node@12/bin;

printf "\n${YELLOW}::>>${NC} Setting up system ...\n\n"
sudo apt-get -yq update;
sudo apt-get -yq upgrade;
sudo apt-get -yq install \
    locales tzdata build-essential make cmake scons curl git autoconf automake gettext libtool flex bison libbz2-dev libcurl4-openssl-dev libexpat-dev libncurses-dev less \
    -o APT::Install-Suggests=0 -o APT::Install-Recommends=0;
sudo localedef -i en_US -f UTF-8 en_US.UTF-8;

pip install --upgrade pip;

# printf "\n${YELLOW}::>>${NC} Installing linuxbrew...\n\n";
yes | /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)";
brew update && brew update;
#brew doctor; # Isn't happy with the environment, but it's from the source image python:3.7-slim-stretch
printf "\n${YELLOW}::>>${NC} Done installing linuxbrew\n";

printf "\n${YELLOW}::>>${NC} Installing jq...\n\n";
brew install jq;
printf "\n${YELLOW}::>>${NC} Done installing jq\n";

printf "\n${YELLOW}::>>${NC} Installing Node.js 12.x...\n\n";
brew install node@12;
printf "\n::Done installing Node.js 12.x\n";

printf "\n${YELLOW}::>>${NC} Installing AWS cli...\n\n";
sudo curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip";
sudo unzip awscliv2.zip;
sudo ./aws/install;
sudo rm -rf awscliv2*;
sudo rm -rf ./aws;
printf "\n::Done installing AWS cli\n";

printf "\n${YELLOW}::>>${NC} Installing [AWS] SAM cli...\n\n";
pip3 install --user aws-sam-cli;
printf "\n${YELLOW}::>>${NC} Done installing [AWS] SAM cli\n";

printf "\n${YELLOW}::>>${NC} Confirming installation. Errors will fail build...\n\n";

VERSION_SAM="`sam --version`";
printf "%s\t == %s\n" "SAM   " $VERSION_SAM;

VERSION_AWS="`aws --version`";
printf "%s\t == %s\n" "AWS   " $VERSION_AWS;

VERSION_NODE="`node --version`";
printf "%s\t == %s\n" "NODE  " $VERSION_NODE;

VERSION_NPM="`npm --version`";
printf "%s\t == %s\n" "NPM   " $VERSION_NPM;

VERSION_JQ="`jq --version`";
printf "%s\t == %s\n" "JQ    " $VERSION_JQ;

VERSION_DOCKER="`docker --version`";
printf "%s\t == %s\n" "DOCKER" $VERSION_DOCKER; # Provided by the context image with dind (Docker in Docker)

printf "\n${YELLOW}::>>${NC} Installation confirmed GOOD!!!\n";
printf "\n${YELLOW}::>>${NC} Cleaning up...\n\n";

sudo apt-get -qy remove build-essential make cmake;
sudo apt-get -qy autoremove;
sudo apt-get -qy clean;
sudo rm -rf /var/lib/apt/lists/* /var/cache/apt/*.bin;

printf "\n${YELLOW}::>>${NC} Exit Code 0\n\n";