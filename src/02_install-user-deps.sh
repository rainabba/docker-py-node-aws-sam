#!/usr/bin/env bash
set -aex;
# source src/color.sh;

export TERM=xterm-color;
export DEBIAN_FRONTEND=noninteractive;
export DEBCONF_NONINTERACTIVE_SEEN=true;
export PATH=/usr/local/bin:/usr/local/sbin:/usr/sbin:/usr/bin:/sbin:/bin:/bin:/root/.local/bin:/home/docker/.local/bin:/home/docker/.yarn/bin:/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:/home/docker/.local/bin:/home/linuxbrew/.linuxbrew/opt/node@12/bin;

printf "\n${YELLOW}::>>${NC} Setting up system ...\n\n"
# apk add --update --no-cache \
#     python3 py3-pip && \
#     ln -sf python3 /usr/bin/python && \
python --version && \
python3 --version && \
python3 -m ensurepip && \
pip install --upgrade pip && \
pip3 install -U --no-cache --upgrade pip setuptools;

PATH=/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:$PATH
USER=linuxbrew
# printf "\n${YELLOW}::>>${NC} Installing linuxbrew...\n\n";
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)" \
	&& HOMEBREW_NO_ANALYTICS=1 brew install -s patchelf \
	&& HOMEBREW_NO_ANALYTICS=1 brew install --ignore-dependencies binutils gmp isl@0.18 libmpc linux-headers mpfr zlib \
	&& (HOMEBREW_NO_ANALYTICS=1 brew install --ignore-dependencies gcc || true) \
	&& HOMEBREW_NO_ANALYTICS=1 brew install glibc \
	&& HOMEBREW_NO_ANALYTICS=1 brew postinstall gcc \
	&& HOMEBREW_NO_ANALYTICS=1 brew remove patchelf \
	&& HOMEBREW_NO_ANALYTICS=1 brew install -s patchelf \
	&& HOMEBREW_NO_ANALYTICS=1 brew config;

brew update && brew update && brew postinstall python3;
#brew doctor; # Isn't happy with the environment, but it's from the source image python:3.7-slim-stretch
printf "\n${YELLOW}::>>${NC} Done installing linuxbrew\n";

printf "\n${YELLOW}::>>${NC} Installing jq...\n\n";
brew install jq;
printf "\n${YELLOW}::>>${NC} Done installing jq\n";

printf "\n${YELLOW}::>>${NC} Installing Node.js 12.x...\n\n";
brew install node@12;
printf "\n::Done installing Node.js 12.x\n";

printf "\n${YELLOW}::>>${NC} Installing AWS cli...\n\n";
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip";
unzip awscliv2.zip;
./aws/install;
rm -rf awscliv2*;
rm -rf ./aws;
printf "\n::Done installing AWS cli\n";

printf "\n${YELLOW}::>>${NC} Installing [AWS] SAM cli...\n\n";
pip3 install --user aws-sam-cli;
printf "\n${YELLOW}::>>${NC} Done installing [AWS] SAM cli\n";

printf "\n${YELLOW}::>>${NC} Confirming installation. Errors will fail build...\n\n";

VERSION_SAM="`sam --version`";
printf "%s\t == %s\n" "SAM   " $VERSION_SAM;

VERSION_AWS="`aws --version`";
printf "%s\t == %s\n" "AWS   " $VERSION_AWS;

VERSION_NODE="`node --version`";
printf "%s\t == %s\n" "NODE  " $VERSION_NODE;

VERSION_NPM="`npm --version`";
printf "%s\t == %s\n" "NPM   " $VERSION_NPM;

VERSION_JQ="`jq --version`";
printf "%s\t == %s\n" "JQ    " $VERSION_JQ;

VERSION_DOCKER="`docker --version`";
printf "%s\t == %s\n" "DOCKER" $VERSION_DOCKER; # Provided by the context image with dind (Docker in Docker)

printf "\n${YELLOW}::>>${NC} Installation confirmed GOOD!!!\n";
printf "\n${YELLOW}::>>${NC} Cleaning up...\n\n";

apk remove build-essential make cmake;
apk autoremove;
apk clean;
rm -rf /var/lib/apt/lists/* /var/cache/apt/*.bin;

printf "\n${YELLOW}::>>${NC} Exit Code 0\n\n";