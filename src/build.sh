#!/usr/bin/env bash
set -eua;

printf "${YELLOW}::>>${NC} GitLab ${CI_BUILD_STAGE} stage starting for ${CI_PROJECT_URL}\n"
printf "${YELLOW}::>>${NC} JobUrl=${CI_JOB_URL}\n"
printf "${YELLOW}::>>${NC} CommitRef=${CI_COMMIT_REF_NAME}\n"
printf "${YELLOW}::>>${NC} CommitMessage=${CI_COMMIT_MESSAGE}\n\n"
printf "${YELLOW}::>>${NC} PWD=$(pwd), CI_REGISTRY=${CI_REGISTRY}\n\n"
buildImage
