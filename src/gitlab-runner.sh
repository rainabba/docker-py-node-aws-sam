#!/usr/bin/env bash
set -ea;
if [ "$#" -ne 1 ]; then
    echo >&2 "Stage not specified. Assuming 'build'"
    JOB_STAGE=build;
else
    JOB_STAGE=${1};
fi


## First run...
#docker run --rm -it -v ~/.gitlab-runner:/etc/gitlab-runner gitlab/gitlab-runner register
#docker run -t --rm --name gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock -v /etc/gitlab-runner:/etc/gitlab-runner gitlab/gitlab-runner run # as a service

# # Commit whatever is staged and ammend (only to be used during feature cycles)
git add -u .;
git commit --amend --no-edit;
sleep 1; # Give the fs a chance to settle before starting the runner

# Manually execute gitlab-runner on our last commit locally
docker run --name=runner-${PWD##*/} \
    -it --rm \
    --env-file $(pwd)/.${ENV}.env \
    -v "/var/run/docker.sock:/var/run/docker.sock" \
    -v "$(pwd)/.gitlab-runner/:/etc/gitlab-runner" \
    -v "$(pwd):$(pwd)" \
    --workdir $(pwd) \
    gitlab/gitlab-runner exec docker \
        $(env|cut -f 2 -d" "|sed 's/^/--env /g'|awk '!/LESS/'|awk '!/PATH/'|awk '!/BROWSER/'|xargs);
        --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
        --docker-privileged \
        --docker-volumes "/certs/client" \
        ${JOB_STAGE};

# function tryStartBgRunner() {
#     # Start runner-as-docker-container to listen for repo changes
#     docker run --name=gitlab-runner-a \
#         -e LISTEN_ADDRESS='0.0.0.0:8093' \
#         --workdir $(pwd) \
#         -t --restart always \
#         --env-file $(pwd)/.${ENV}.env \
#         -v "$(pwd)/.gitlab-runner/:/etc/gitlab-runner" \
#         -v "/var/run/docker.sock:/var/run/docker.sock" \
#         -v "$(pwd):$(pwd)" \
#         -v "$(pwd)/.${ENV}.env:${CI_PROJECT_DIR}/.${ENV}.env" \
#         gitlab/gitlab-runner run --listen-address='0.0.0.0:8093';
# }