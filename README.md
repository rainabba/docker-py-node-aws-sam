# docker-py-node-aws-sam

A docker image to provide a stable environment (for use locally and on GitLab) with Python 3, Node.js 12.x, AWS CLI v2, and AWS SAM cli. When env-vars are setup as below, you should be able to go from a working aws-cli setup (or follow directions to establish `~/.aws/` properly), to a Docker container with those same credentials and the basic tools needed to clone a project, build an image, then publish that image. Hosted with GitLab, this same structure should allow for fast CI/CD of images to a repo (DockerHub and AWS code already provided.)

GitLab Repo: [https://github.com/rainabba/docker-py-node-aws-sam](https://github.com/rainabba/docker-py-node-aws-sam)

DockerHub Images: [https://hub.docker.com/repository/docker/rainabba/py-node-aws-sam](https://hub.docker.com/repository/docker/rainabba/py-node-aws-sam)

[Keep a Changelog](https://keepachangelog.com/en/1.0.0/): This project includes a CHANGELOG.md and adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)

## ** WARNING**

The `main` branch of this project drives a continuous deployment pipline which will attempt to build and publish successful builds when code is pushed to the master branch. See the [Continuous Integration and Continuous Deployment](#Continuous-Integration-and-Continuous-Deployment) section below for more info.

## Description

This project depends heavily on using the aws cli (see above), sam cli and permissions to do a `sam deploy` to the appropriate account.

Includes:

- Python == 3.7
- SAM CLI == SAM CLI, version 0.48.0
- AWS CLI == aws-cli/2.0.12 Python/3.7.3 Linux/4.19.84-microsoft-standard botocore/2.0.0dev16
- JQ == jq-1.6
- NVM == 0.38.0 (node LTS by default)
- Java == 1.8
- DOCKER == Docker version 19.03.8, build afacb8b7f0
- BREW == TODO
- PIP3 == TODO

### Load Environment

```.env
IMAGE_NAME=py-node-aws-sam
CI_REGISTRY_USER=rainabba
CI_REGISTRY_IMAGE=docker-py-node-aws-sam
CI_PROJECT_DIR=/builds/rainabba/docker-py-node-aws-sam
```

### .preinstall.sh script

If there are commands that you'd like to run early in the container init process, mount a shell script file to `/home/docker/src/.preinstall.sh` (see below volume example)

```bash
  docker run
  ...
  -v ${PWD}/.preinstall.sh:/home/docker/src/.preinstall.sh \
```

### Clone the project

```bash
git clone https://gitlab.com/rainabba/docker-py-node-aws-sam.git
```

```bash
source ./src/01_prepare-environment.sh; # Prepare environment to load scripts
buildImage && runContainer; # Build and Run
publishImage; # Publish
```